package com.scania.saib5g.fakeapi.dataaccess;

import com.scania.saib5g.fakeapi.models.Guitar;
import com.scania.saib5g.fakeapi.models.GuitarDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class GuitarRepository implements GuitarRepositoryInterface {

    private Map<String, Integer> brands = seedBrands();
    private Map<Integer, Guitar> guitars = seedGuitars();


    private HashMap<Integer, Guitar> seedGuitars() {
        var guitars = new HashMap<Integer, Guitar>();

        guitars.put(1, new Guitar(1, "Fender", "Telecaster"));
        guitars.put(2, new Guitar(2, "Fender", "Stratocaster"));
        guitars.put(3, new Guitar(3, "Gibson", "Les Paul"));

        return guitars;
    }

    private HashMap<String, Integer> seedBrands() {
        var brands = new HashMap<String, Integer>();

        brands.put("Fender", 2);
        brands.put("Gibson", 1);

        return brands;
    }

    @Override
    public Map<Integer, Guitar> getAllGuitars() {
        return guitars;
    }

    @Override
    public Guitar getGuitar(int id) {
        return guitars.get(id);
    }

    @Override
    public Guitar addGuitar(Guitar guitar) {
        addBrand(guitar.getBrand());
        guitars.put(guitar.getId(), guitar);

        return getGuitar(guitar.getId());
    }

    @Override
    public Guitar replaceGuitar(Guitar guitar) {
        var guitarToReplace = getGuitar(guitar.getId());

        removeBrand(guitarToReplace.getBrand());
        guitars.remove(guitarToReplace);
        addBrand(guitar.getBrand());
        guitars.put(guitar.getId(), guitar);

        return getGuitar(guitar.getId());
    }

    @Override
    public Guitar modifyGuitar(Guitar guitar) {
        var guitarToModify = getGuitar(guitar.getId());

        removeBrand(guitarToModify.getBrand());
        guitarToModify.setBrand(guitar.getBrand());
        guitarToModify.setModel(guitar.getModel());
        addBrand(guitar.getBrand());

        return getGuitar(guitar.getId());
    }

    @Override
    public void deleteGuitar(int id) {
        removeBrand(getGuitar(id).getBrand());
        guitars.remove(id);
    }

    @Override
    public boolean guitarExistsInRepo(int id) {
        return guitars.containsKey(id);
    }

    @Override
    public boolean isValidGuitar(Guitar guitar) {
        return guitar.getId() > 0 && guitar.getBrand() != null && guitar.getModel() != null;
    }

    @Override
    public boolean isValidGuitar(Guitar guitar, int id) {
        return isValidGuitar(guitar) && guitar.getId() == id;
    }

    public void addBrand (String brand) {
        if (brands.containsKey(brand)) {
            brands.put(brand, brands.get(brand) + 1);
        } else {
            brands.put(brand, 1);
        }
    }

    public void removeBrand (String brand) {
        if (brands.get(brand) > 1) {
            brands.put(brand, brands.get(brand) - 1);
        } else {
            brands.remove(brand);
        }
    }

    public GuitarDto getTotalGuitarsPerBrand () {
        return new GuitarDto(brands);
    }
}
