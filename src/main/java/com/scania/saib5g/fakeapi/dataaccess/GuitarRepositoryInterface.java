package com.scania.saib5g.fakeapi.dataaccess;

import com.scania.saib5g.fakeapi.models.Guitar;

import java.util.Map;

public interface GuitarRepositoryInterface {
    Map<Integer, Guitar> getAllGuitars();
    Guitar getGuitar(int id);
    Guitar addGuitar(Guitar guitar);
    Guitar replaceGuitar(Guitar guitar);
    Guitar modifyGuitar(Guitar guitar);
    void deleteGuitar(int id);
    boolean guitarExistsInRepo(int id);
    boolean isValidGuitar(Guitar guitar);
    boolean isValidGuitar(Guitar guitar, int id);
}
