package com.scania.saib5g.fakeapi.controllers;

import com.scania.saib5g.fakeapi.dataaccess.GuitarRepository;
import com.scania.saib5g.fakeapi.models.Guitar;
import com.scania.saib5g.fakeapi.models.GuitarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping(value = "/v1/api/guitars")
public class GuitarController {
    private GuitarRepository guitarRepository;

    @Autowired
    public GuitarController(GuitarRepository guitarRepository) {
        this.guitarRepository = guitarRepository;
    }


    @GetMapping
    public ResponseEntity<Map<Integer, Guitar>> getAllGuitars() {
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExistsInRepo(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Guitar> createGuitar(@RequestBody Guitar guitar) {
        if (!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(guitarRepository.addGuitar(guitar), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Guitar> replaceGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        if (!guitarRepository.isValidGuitar(guitar, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!guitarRepository.guitarExistsInRepo(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(guitarRepository.replaceGuitar(guitar), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Guitar> modifyGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        if (!guitarRepository.isValidGuitar(guitar, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!guitarRepository.guitarExistsInRepo(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(guitarRepository.modifyGuitar(guitar), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExistsInRepo(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        guitarRepository.deleteGuitar(id);
        return new ResponseEntity<>("Guitar with id " + id + " has been deleted", HttpStatus.OK);
    }

    @GetMapping("/brands")
    public ResponseEntity<GuitarDto> getGuitarsPerBrand () {
        return new ResponseEntity<>(guitarRepository.getTotalGuitarsPerBrand(), HttpStatus.OK);
    }


}
