package com.scania.saib5g.fakeapi.models;

import java.util.Map;

public class GuitarDto {

    private Map<String, Integer> totalGuitarsPerBrand;

    public GuitarDto(Map<String, Integer> totalGuitarsPerBrand) {
        this.totalGuitarsPerBrand = totalGuitarsPerBrand;
    }

    public Map<String, Integer> getTotalGuitarsPerBrand() {
        return totalGuitarsPerBrand;
    }

    public void setTotalGuitarsPerBrand(Map<String, Integer> totalGuitarsPerBrand) {
        this.totalGuitarsPerBrand = totalGuitarsPerBrand;
    }
}
